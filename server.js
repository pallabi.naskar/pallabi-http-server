const http = require("http");
const fs = require("fs");
var port = 5000;

const { v4: uuIdv4 } = require("uuid");

const readFilePromise = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

const errorHandlerCallback = (request, response, err) => {
  console.error(err);
  response.writeHead(500);
  response.write("Internal server Error/invalid path");
  response.end();
};

const server = http.createServer((request, response, err) => {
  console.log(request.url);
  let givenUrl = request.url;
  let splitUrl = givenUrl.split("/");
  console.log(splitUrl);

  if (splitUrl[1] === "GET") {
    switch (splitUrl[2]) {
      /**
       * GET /json - return the following json content
       */
      case "json": {
        readFilePromise("./get_json.json")
          .then((data) => {
            response.writeHead(200, {
              "Content-Type": "text/json",
            });
            response.write(data);
            response.end();
          })
          .catch((err) => {
            errorHandlerCallback(request, response, err);
          });

        break;
      }
      /**
       * GET /html - return the following HTML content
       */

      case "html": {
        readFilePromise("./get_html.html")
          .then((data) => {
            response.writeHead(200, {
              "Content-Type": "text/html",
            });
            response.write(data);
            response.end();
          })
          .catch((err) => {
            errorHandlerCallback(request, response, err);
          });

        break;
      }
      /**
       * GET /uuid - return the following uuid content
       */

      case "uuid": {
        try {
          const uuidValue = uuIdv4();
          if (uuidValue !== undefined) {
            response.writeHead(200, {
              "Content-Type": "text/json",
            });
            const uuidValue = JSON.stringify(uuIdv4());

            response.write(`{uuid :${uuidValue}}`);
            response.end();
          } else {
            throw err;
          }
        } catch (err) {
          errorHandlerCallback(request, response, err);
        }
        break;
      }
      /**
       * GET /status/{status_code} - return a response with a status code as specified in the request.
       */

      case "status": {
        let codeValue = parseInt(splitUrl[3]);
        try {
          codeValue1 = http.STATUS_CODES[codeValue];
          if (codeValue1 !== undefined) {
            response.writeHead(codeValue, {
              "Content-Type": "text/json",
            });
            console.log(codeValue1);
            response.write(`${splitUrl[3]}: ${codeValue1}`);
            response.end();
          } else {
            throw err;
          }
        } catch (err) {
          errorHandlerCallback(request, response, err);
        }
        break;
      }
      /**
       * GET /delay/{delay_in_seconds} -return a success response but after the specified delay in the request.
       */

      case "delay": {
        let delayValue = parseInt(splitUrl[3]);
        try {
          if (Number.isInteger(delayValue) == true && delayValue >= 0) {
            response.writeHead(200, {
              "Content-Type": "text/json",
            });
            setTimeout(() => {
              response.write("success");
              response.end();
            }, delayValue * 1000);
          } else {
            throw err;
          }
        } catch (err) {
          errorHandlerCallback(request, response, err);
        }
        break;
      }

      default: {
        response.writeHead(404, {
          "Content-Type": "text/json",
        });
        response.write("No page is found");
        response.end();
        break;
      }
    }
  } else {
    console.log("page ERROR");
  }
});
server.listen(port);
